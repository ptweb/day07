<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Student List</title>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <style>
    .background {
      width: 55rem;
      margin: auto;
      margin-top: 2rem;
      padding: 2rem 2rem;
      align-items: center;
      border-radius: 8px;
      border: solid 2px #4e7aa3;
    }

    .form {
      width: 100%;
    }

    .search_layout {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .layout {
      display: flex;
      margin-bottom: 20px;
      align-items: center;
      font-size: 16px;
    }

    .search_label {
      width: 85px;
      padding: 5px 6px;
      font-size: 16px;
    }

    .search_input {
      width: 60%;
      outline: none;
      height: 3.8rem;
      padding-left: 0.5rem;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    select {
      width: 60%;
      outline: none;
      height: 3.8rem;
      font-size: 16px;
      padding-left: 0.5rem;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    .button {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .btn-submit {
      width: 11rem;
      height: 3.8rem;
      cursor: pointer;
      color: white;
      font-size: 16px;
      background-color: #5b9bd5;
      border-radius: 8px;
      border: solid 2px #4e7aa3;
    }

    .btn-add {
      width: 10rem;
      height: 3.8rem;
      color: white;
      background-color: #5b9bd5;
      border-radius: 8px;
      border: solid 2px #4e7aa3;
    }

    .btn-action {
      height: 2.8rem;
      width: 6rem;
      color: white;
      background-color: #5b9bd5;
      border: solid 1px #4e7aa3;
    }

    .add {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-top: 30px;
      font-size: 16px;
    }

    table {
      width: 100%;
      font-size: 16px;
    }

    td,
    th {
      text-align: left;
      padding: 10px;
      border: 1px solid #4e7aa3;
    }

    .student-list {
      margin-top: 20px;
    }
  </style>
</head>

<body>
  <div class="background">
    <?php
    session_start();
    if ($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['navigateToSignUp'])) {
      header("Location: signup.php");
    }
    ?>
    <div class="form">
      <div class="search_layout">
        <form>
          <div class="layout">
            <p class="search_label">Khoa</p>
            <select name="department">
              <?php
              $department = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
              foreach ($department as $key => $value) {
                echo '<option >' . $value . '</option>';
              }
              ?>
            </select>
          </div>

          <div class="layout">
            <p for="search" class="search_label">Từ khóa</p>
            <input type="text" name="search" id="search" class="search_input" />
          </div>

          <div class="button">
            <button type="submit" class="btn-submit">Tìm kiếm</button>
          </div>
        </form>
      </div>

      <div class="add">
        <div>
          <p>Số sinh viên tìm thấy: xxx</p>
        </div>

        <div>
          <form method="post">
            <input type="submit" class="btn-add" name="navigateToSignUp" value="Thêm" />
          </form>
        </div>
      </div>

      <div class="student-list">
        <table>
          <tr text-align="center">
            <td>No</td>
            <td>Tên sinh viên</td>
            <td>Khoa</td>
            <td>Action</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Nguyễn Văn A</td>
            <td>Khoa học máy tính</td>
            <td>
              <button class="btn-action">Xóa</button>
              <button class="btn-action">Sửa</button>
            </td>
          </tr>
          <tr>
            <td>2</td>
            <td>Trần Thị B</td>
            <td>Khoa học máy tính</td>
            <td>
              <button class="btn-action">Xóa</button>
              <button class="btn-action">Sửa</button>
            </td>
          </tr>
          <tr>
            <td>3</td>
            <td>Nguyễn Hoàng C</td>
            <td>Khoa học vật liệu</td>
            <td>
              <button class="btn-action">Xóa</button>
              <button class="btn-action">Sửa</button>
            </td>
          </tr>
          <tr>
            <td>4</td>
            <td>Đinh Quang D</td>
            <td>Khoa học vật liệu</td>
            <td>
              <button class="btn-action">Xóa</button>
              <button class="btn-action">Sửa</button>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</body>

</html>